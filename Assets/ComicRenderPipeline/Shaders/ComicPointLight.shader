﻿Shader "Comic/Lighting/ComicPointLight"
{
	SubShader
	{
		Pass
		{
		Tags { "RenderType" = "Opaque" }
			ZTest Always
			ZWrite Off
			Blend One One

			HLSLPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Common.hlsl"

			CBUFFER_START(UnityPerFrame)
				float4x4 unity_MatrixVP;

				float4 _ScreenParams;
			CBUFFER_END

			CBUFFER_START(UnityPerDraw)
				float4x4 unity_ObjectToWorld;
			CBUFFER_END

			#define UNITY_MATRIX_M unity_ObjectToWorld


			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 worldPos : TEXCOORD1;
			};

			float4 _Color;
			float _Range;
			float _Intensity;
			float4 _Position;
			float4 _ZBufferParams;
			float4 _CameraPos;
			float4 _CameraForward;

			TEXTURE2D(_nBuffer);
			SAMPLER(sampler_nBuffer);

			TEXTURE2D_FLOAT(_dBuffer);
			SAMPLER(sampler_dBuffer);

			v2f vert(appdata v,
				out float4 vertex : SV_POSITION)
			{
				v2f o;
				o.worldPos = mul(UNITY_MATRIX_M, float4(v.vertex.xyz, 1.0));
				vertex = mul(unity_MatrixVP, o.worldPos);
				vertex.z = min(0, vertex.z);
				o.uv = v.uv;
				return o;
			}

			float4 frag(v2f i, float4 vpos : VPOS) : SV_Target
			{
				// sample the texture
				float2 screenUV = vpos.xy / _ScreenParams.xy;
				float4 normal = SAMPLE_TEXTURE2D(_nBuffer, sampler_nBuffer, screenUV);
				float depth = LinearEyeDepth(SAMPLE_DEPTH_TEXTURE(_dBuffer, sampler_dBuffer, screenUV), _ZBufferParams);
				
				float3 camDirection = normalize(i.worldPos.xyz-_CameraPos.xyz);
				camDirection = camDirection / dot(camDirection, _CameraForward);
				float3 fragmentPos = _CameraPos.xyz + depth * camDirection;
				float3 posDif = fragmentPos-_Position;
				float3 dir = normalize(posDif);
				//dir = dir / dot(dir, _CameraForward.xyz);

				return saturate(dot((2 * normal) - 1, -dir)) * _Color * _Intensity * lerp(1, 0, length(posDif) / _Range);
			}
			ENDHLSL
		}
	}
}
