﻿Shader "Comic/Lighting/ComicShadows"
{
	Properties
	{
		_Threshold("Threshold", Float) = 0.5
		_Tile("Tile Amount", Float) = 1
		_Dither("Dither Map", 2D) = "White" {}
	}
		SubShader
	{
		Pass
		{
			ZTest Always
			ZWrite Off
			Cull Off
			HLSLPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Common.hlsl"

			CBUFFER_START(UnityPerFrame)
				float4x4 unity_MatrixVP;

				float4 _ScreenParams;
			CBUFFER_END

			CBUFFER_START(UnityPerDraw)
				float4x4 unity_ObjectToWorld;
			CBUFFER_END

			#define UNITY_MATRIX_M unity_ObjectToWorld


			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
			};

			float4 _Direction;
			float _Tile;
			float _Threshold;
			float _ShadowSteps;

			TEXTURE2D(_sBuffer);
			SAMPLER(sampler_sBuffer);

			TEXTURE2D(_Dither);
			SAMPLER(sampler_Dither);

			v2f vert(appdata v,
				out float4 vertex : SV_POSITION)
			{
				v2f o;
				vertex = float4(v.vertex.xy, 0, 0.5);
				o.uv = v.uv;
				return o;
			}

			float4 frag(v2f i, float4 vpos : VPOS) : SV_Target
			{
				// sample the texture
				float interval = 1 / _ShadowSteps;
				float4 light = 1-SAMPLE_TEXTURE2D(_sBuffer, sampler_sBuffer, i.uv);
				float4 shade = SAMPLE_TEXTURE2D(_Dither, sampler_Dither, vpos.xy / _ScreenParams.y * 2 * _Tile);
				
				light = light * shade;
				light = 1 - light;

				float3 normLight = normalize(light.xyz);
				float brightness = (0.299*light.r + 0.587*light.g + 0.114*light.b);
				float lowEdge = trunc(brightness / interval) * interval;
				float highEdge = lowEdge + interval;
				float alpha = (brightness-lowEdge) / interval;
				alpha = step(0.5, alpha);
				light = light * (lerp(lowEdge, highEdge, alpha) / brightness);
				return light;
			}
			ENDHLSL
		}
	}
}
