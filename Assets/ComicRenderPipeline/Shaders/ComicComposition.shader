﻿Shader "Comic/Lighting/ComicDirectionalLight"
{
	SubShader
	{
		Pass
		{
			ZTest Always
			ZWrite Off
			Cull Off
			HLSLPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Common.hlsl"

			CBUFFER_START(UnityPerFrame)
				float4x4 unity_MatrixVP;
			CBUFFER_END

			CBUFFER_START(UnityPerDraw)
				float4x4 unity_ObjectToWorld;
			CBUFFER_END

			#define UNITY_MATRIX_M unity_ObjectToWorld


			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			float4 _Direction;

			TEXTURE2D(_cBuffer);
			SAMPLER(sampler_cBuffer);
			TEXTURE2D(_sBuffer);
			SAMPLER(sampler_sBuffer);

			v2f vert(appdata v)
			{
				v2f o;
				o.vertex = float4(v.vertex.xy, 0, 0.5);
				o.uv = v.uv;

				return o;
			}

			float4 frag(v2f i) : SV_Target
			{
				// sample the texture
				float4 color = SAMPLE_TEXTURE2D(_cBuffer, sampler_cBuffer, i.uv);
				float4 shadow = SAMPLE_TEXTURE2D(_sBuffer, sampler_sBuffer, i.uv);
				return color*shadow;
			}
			ENDHLSL
		}
	}
}
