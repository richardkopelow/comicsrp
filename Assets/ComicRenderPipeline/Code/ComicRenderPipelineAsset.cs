﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

[CreateAssetMenu]
public class ComicRenderPipelineAsset : RenderPipelineAsset
{
    public Shader DirectionalLightShader;
    public Shader PointLightShader;
    public Shader CompositionShader;
    public Material ComicShadows;

    [Space]
    public int ShadowSteps;

    protected override RenderPipeline CreatePipeline()
    {
        return new ComicRenderPipeline(this);
    }
}
