﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class ComicRenderPipeline : RenderPipeline
{
    public const string cBufferName = "_cBuffer";
    public const string nBufferName = "_nBuffer";
    public const string dBufferName = "_dBuffer";
    public const string sBufferName = "_sBuffer";

    private ComicRenderPipelineAsset settings;

    private CommandBuffer commandBuffer = new CommandBuffer();
    private RenderTexture cBuffer;
    private RenderTexture nBuffer;
    private RenderTexture dBuffer;
    private RenderTexture sBuffer;
    private RenderTargetIdentifier[] buffers = new RenderTargetIdentifier[2];
    private Material compositMat;

    public ComicRenderPipeline(ComicRenderPipelineAsset settings)
    {
        this.settings = settings;
        compositMat = new Material(settings.CompositionShader);
    }

    protected override void Render(ScriptableRenderContext context, Camera[] cameras)
    {
        for (int i = 0; i < cameras.Length; i++)
        {
            Camera camera = cameras[i];

            cBuffer = RenderTexture.GetTemporary(camera.pixelWidth, camera.pixelHeight, 0, RenderTextureFormat.Default, RenderTextureReadWrite.Default);
            nBuffer = RenderTexture.GetTemporary(camera.pixelWidth, camera.pixelHeight, 0, RenderTextureFormat.Default, RenderTextureReadWrite.Default);
            dBuffer = RenderTexture.GetTemporary(camera.pixelWidth, camera.pixelHeight, 16, RenderTextureFormat.Depth, RenderTextureReadWrite.Default);
            sBuffer = RenderTexture.GetTemporary(camera.pixelWidth, camera.pixelHeight, 0, RenderTextureFormat.ARGB32, RenderTextureReadWrite.Default);
            buffers[0] = cBuffer;
            buffers[1] = nBuffer;

            Render(context, camera);


            RenderTexture.ReleaseTemporary(cBuffer);
            RenderTexture.ReleaseTemporary(nBuffer);
            RenderTexture.ReleaseTemporary(dBuffer);
            RenderTexture.ReleaseTemporary(sBuffer);
        }
    }

    private void Render(ScriptableRenderContext context, Camera camera)
    {
        context.SetupCameraProperties(camera, camera.stereoEnabled);

        ScriptableCullingParameters cullingParameters;
        if (!camera.TryGetCullingParameters(out cullingParameters))
        {
            return;
        }
        var cullResults = context.Cull(ref cullingParameters);

        // Set Global Values
        commandBuffer.SetGlobalFloat("_ShadowSteps", settings.ShadowSteps);

        // Init buffers
        commandBuffer.name = "Init G-Buffers";
        commandBuffer.SetRenderTarget(buffers, dBuffer);
        commandBuffer.ClearRenderTarget(
            true,
            true,
            camera.backgroundColor
            );
        context.ExecuteCommandBuffer(commandBuffer);
        commandBuffer.Clear();

        // Fill G-Buffers
        {
            var drawSettings = new DrawingSettings(new ShaderTagId("Comic"), new SortingSettings(camera));
            var filterSettings = new FilteringSettings(RenderQueueRange.opaque, ~0);
            context.DrawRenderers(cullResults, ref drawSettings, ref filterSettings);

            filterSettings = new FilteringSettings(RenderQueueRange.transparent, ~0);
            context.DrawRenderers(cullResults, ref drawSettings, ref filterSettings);
        }

        // Copy Depth buffer
        RenderTexture depthCopy = RenderTexture.GetTemporary(camera.pixelWidth, camera.pixelHeight, 0, RenderTextureFormat.RFloat, RenderTextureReadWrite.Default);
        commandBuffer.SetGlobalTexture(dBufferName, dBuffer);
        commandBuffer.SetRenderTarget(depthCopy);
        Material depthCopyMat = new Material(Shader.Find("Hidden/Util/CopyDepth"));
        QuadBlit(commandBuffer, depthCopyMat);
        commandBuffer.SetGlobalTexture(dBufferName, depthCopy);


        // Set Buffers as global textures
        commandBuffer.name = "Push Buffers";
        commandBuffer.SetGlobalTexture(cBufferName, cBuffer);
        commandBuffer.SetGlobalTexture(nBufferName, nBuffer);
        commandBuffer.SetGlobalTexture(dBufferName, depthCopy);
        context.ExecuteCommandBuffer(commandBuffer);
        commandBuffer.Clear();

        // Render Lighting
        commandBuffer.name = "Render Lights";
        commandBuffer.SetRenderTarget(sBuffer, dBuffer);
        commandBuffer.ClearRenderTarget(false, true, Color.black);

        float f = camera.farClipPlane;
        float n = camera.nearClipPlane;
        Vector4 zBufferParams = new Vector4(-1 + f / n, 1, -1 / f + 1 / n, 1 / f);//new Vector4(1 - f / n, f / n, (1 - f / n) / f, 1 / f);
        commandBuffer.SetGlobalVector("_CameraPos", camera.transform.position);
        commandBuffer.SetGlobalVector("_CameraForward", camera.transform.forward);
        commandBuffer.SetGlobalVector("_ZBufferParams", zBufferParams);
        foreach (var light in cullResults.visibleLights)
        {
            switch (light.lightType)
            {
                case LightType.Spot:
                    break;
                case LightType.Directional:
                    {
                        Material directionalMat = new Material(settings.DirectionalLightShader);
                        directionalMat.SetVector("_Direction", -light.light.transform.forward);
                        directionalMat.SetColor("_Color", light.light.color);
                        directionalMat.SetFloat("_Intensity", light.light.intensity);

                        QuadBlit(commandBuffer, directionalMat);
                    }
                    break;
                case LightType.Point:
                    {
                        Material pointMat = new Material(settings.PointLightShader);
                        pointMat.SetVector("_Position", light.light.transform.position);
                        pointMat.SetFloat("_Range", light.light.range);
                        pointMat.SetColor("_Color", light.light.color);
                        pointMat.SetFloat("_Intensity", light.light.intensity);

                        Mesh sphere = new Mesh();
                        PrimitiveMeshs.BuildSphere(ref sphere, light.light.range, 16, 8);
                        commandBuffer.DrawMesh(sphere, light.localToWorldMatrix, pointMat);
                    }
                    break;
                case LightType.Area:
                    break;
                case LightType.Disc:
                    break;
                default:
                    break;
            }
        }

        //ModShadows
        RenderTexture moddedShadows = RenderTexture.GetTemporary(sBuffer.descriptor);
        commandBuffer.SetGlobalTexture(sBufferName, sBuffer);
        commandBuffer.SetRenderTarget(moddedShadows);
        QuadBlit(commandBuffer, settings.ComicShadows);
        commandBuffer.SetGlobalTexture(sBufferName, moddedShadows);

        context.ExecuteCommandBuffer(commandBuffer);
        commandBuffer.Clear();

        // Blit to Final Target;
        RenderTexture composit = RenderTexture.GetTemporary(cBuffer.descriptor);
        commandBuffer.name = "Composit";
        commandBuffer.SetRenderTarget(
            composit,
            dBuffer.depthBuffer);
        commandBuffer.ClearRenderTarget(false, true, Color.black);
        QuadBlit(commandBuffer, compositMat);
        //commandBuffer.Blit(null, BuiltinRenderTextureType.CameraTarget, compositMat);
        context.ExecuteCommandBuffer(commandBuffer);
        context.DrawSkybox(camera);
        commandBuffer.Clear();

        commandBuffer.name = "Blit to cam";
        commandBuffer.Blit(composit, BuiltinRenderTextureType.CameraTarget);
        context.ExecuteCommandBuffer(commandBuffer);
        commandBuffer.Clear();

        context.Submit();
        RenderTexture.ReleaseTemporary(moddedShadows);
        RenderTexture.ReleaseTemporary(composit);
        RenderTexture.ReleaseTemporary(depthCopy);
    }

    private void QuadBlit(CommandBuffer commandBuffer, Material blitMat)
    {
        Mesh fsQuad = new Mesh();
        PrimitiveMeshs.BuildFullScreenQuad(ref fsQuad);
        commandBuffer.DrawMesh(fsQuad, Matrix4x4.identity, blitMat);
    }
}
